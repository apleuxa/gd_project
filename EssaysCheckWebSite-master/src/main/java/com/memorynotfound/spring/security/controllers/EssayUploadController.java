package com.memorynotfound.spring.security.controllers;

import com.google.common.collect.Lists;
import com.memorynotfound.spring.security.domain.TextChecker;
import com.memorynotfound.spring.security.model.Essay;
import com.memorynotfound.spring.security.model.User;
import com.memorynotfound.spring.security.repository.EssayRepository;
import com.memorynotfound.spring.security.service.UserServiceImpl;
import com.memorynotfound.spring.security.utils.FileUtils;
import com.memorynotfound.spring.security.utils.UserUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.Multipart;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/userUploadEssay")
public class EssayUploadController {

    private final UserServiceImpl userService;

    private final EssayRepository essayRepository;

    private final String PATH = "/Users/mukhin/Desktop/files/";

    public EssayUploadController(UserServiceImpl userService, EssayRepository essayRepository) {
        this.userService = userService;
        this.essayRepository = essayRepository;
    }

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public String showEssayUploadPage(Model model) {

        Optional<User> userHolder = UserUtils.getCurrentAuthenticatedUser(userService);

        if(userHolder.isPresent()) {

            User user = userHolder.get();

            model.addAttribute("email", user.getEmail());
            model.addAttribute("name", String.format("%s %s", user.getFirstName(), user.getLastName()));
            model.addAttribute("numberOfTries", user.getNumberOfTries());
        }

        return "userUploadEssay";
    }

    @PostMapping(value = "/upload")
    public String uploadEssay(@RequestParam("file") MultipartFile file){
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                Optional<User> userHolder = UserUtils.getCurrentAuthenticatedUser(userService);

                if(userHolder.isPresent()){

                    User user = userHolder.get();

                    String filename = FileUtils.generateFileName(file.getOriginalFilename(), user);

                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(PATH + filename)));
                    stream.write(bytes);
                    stream.close();

                    Essay essay = new Essay();

                    String text = new String(bytes);

                    List<Boolean> checks = checkText(text);

                    essay.setFilename(filename);
                    essay.setTryNumber(user.getNumberOfTries() + 1);

                    essay.setFirstStep(checks.get(0));
                    essay.setSecondStep(checks.get(1));
                    essay.setThirdStep(checks.get(2));
                    essay.setFourthStep(checks.get(3));
                    essay.setFifthStep(checks.get(4));

                    essay.setUserId(user.getId());

                    essayRepository.save(essay);

                    userService.incrementUserNumberOfTries(user.getId());

                    return "redirect:/userUploadEssay?success";
                }
            } catch (Exception e) {
                return "redirect:/userUploadEssay?error";
            }
        }

        return "redirect:/userUploadEssay?error";
    }

    private List<Boolean> checkText(String text){
        TextChecker checker = new TextChecker(text);

        return Lists.newArrayList(
                checker.checkFirstStep(), checker.checkSecondStep(), checker.checkThirdStep(),
                checker.checkFourthStep(), checker.checkFifthStep()
        );
    }
}
