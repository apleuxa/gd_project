package com.memorynotfound.spring.security.utils;

import java.util.concurrent.ThreadLocalRandom;

public class PasswordUtils {

    public static String generatePassword(int length) {
        StringBuilder s = new StringBuilder();

        s.append((char) ThreadLocalRandom.current().nextInt(65, 90));

        for (int i = 1; i < length; i++) {
            s.append((char) ThreadLocalRandom.current().nextInt(33, 122));
        }

        return s.toString();
    }

}
