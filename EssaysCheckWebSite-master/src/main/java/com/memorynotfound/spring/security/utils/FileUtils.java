package com.memorynotfound.spring.security.utils;

import com.memorynotfound.spring.security.model.User;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileUtils {

    public static String getFileExtension(String fileName) {
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf("."));
        return "";
    }

    public static String generateFileName(String innerName, User user) {
        StringBuilder sb = new StringBuilder();

        String ext = getFileExtension(innerName);
        sb.append(innerName, 0, innerName.lastIndexOf('.'));

        if (user != null) {
            String userAsString = user.toString();

            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] messageDigest = md.digest(userAsString.getBytes());
                BigInteger no = new BigInteger(1, messageDigest);
                StringBuilder hashTextBuilder = new StringBuilder(no.toString(16));
                while (hashTextBuilder.length() < 32) {
                    hashTextBuilder.insert(0, "0");
                }
                sb.append(hashTextBuilder);
                sb.append(String.format("_%d", user.getNumberOfTries()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        sb.append(ext);

        return sb.toString();
    }
}
