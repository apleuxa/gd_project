package com.memorynotfound.spring.security.controllers;

import com.memorynotfound.spring.security.mail.MailSender;
import com.memorynotfound.spring.security.model.User;
import com.memorynotfound.spring.security.service.UserService;
import com.memorynotfound.spring.security.utils.PasswordUtils;
import com.memorynotfound.spring.security.web.dto.ManagerRegistrationDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final int GENERATED_PASSWORD_LENGTH = 8;

    private final UserService userService;

    private final MailSender mailSender;

    public AdminController(UserService userService, MailSender mailSender) {
        this.userService = userService;
        this.mailSender = mailSender;
    }

    @ModelAttribute("manager")
    @PreAuthorize("hasRole('ADMIN')")
    public ManagerRegistrationDto managerRegistrationDto() {
        return new ManagerRegistrationDto();
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public String showAdminPage() {
        return "admin";
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView registerManagerAccount(@ModelAttribute("manager") ManagerRegistrationDto managerDto, BindingResult result) {
        User existing = userService.findByEmail(managerDto.getEmail());

        if (existing != null) {
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        ModelAndView model = new ModelAndView();

        String password = PasswordUtils.generatePassword(GENERATED_PASSWORD_LENGTH);
        managerDto.setPassword(password);
        model.addObject("password", password);

        if (result.hasErrors()) {
            model.setViewName("admin");
            return model;
        }

        userService.saveManager(managerDto);

        mailSender.sendEmail(managerDto.getEmail(), "Password for GD Project", String.format("Dear, %s %s!\nYour password: %s",
                managerDto.getFirstName(), managerDto.getLastName(), password));

        model.setViewName("redirect:/admin?success");
        return model;
    }

    @GetMapping("/adminPanel")
    @PreAuthorize("hasRole('ADMIN')")
    public String showAdminPanel(Model model){

        model.addAttribute("users", userService.allUsers());
        model.addAttribute("roles", userService.allUsers().stream().map(User::getNiceRole).collect(Collectors.toList()));

        return "adminPanel";
    }

    @PostMapping("/adminPanel/delete/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteUser(@PathVariable(name = "userId") Long userId){

        User user = userService.findById(userId);

        if(user != null && user.getRoles().stream().noneMatch(it -> it.getName().equals("ROLE_ADMIN"))){
            System.out.println(userId + " deleted");
            userService.delete(userId);
        }

        return "redirect:/admin/adminPanel";
    }

}
