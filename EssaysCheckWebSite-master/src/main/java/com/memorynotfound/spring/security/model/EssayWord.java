package com.memorynotfound.spring.security.model;

import javax.persistence.*;

@Entity
public class EssayWord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String word;

    public EssayWord(){

    }

    public EssayWord(String word){
        this.word = word;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
