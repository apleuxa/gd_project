package com.memorynotfound.spring.security.service;

import com.google.common.collect.Lists;
import com.memorynotfound.spring.security.repository.RoleRepository;
import com.memorynotfound.spring.security.model.Role;
import com.memorynotfound.spring.security.model.User;
import com.memorynotfound.spring.security.repository.UserRepository;
import com.memorynotfound.spring.security.web.dto.ManagerRegistrationDto;
import com.memorynotfound.spring.security.web.dto.UserRegistrationDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    @Override
    public User saveUser(UserRegistrationDto registration){
        User user = new User();
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));

        Role role = roleRepository.findByName("ROLE_USER");

        user.setRoles(Collections.singletonList(role == null ? new Role("ROLE_USER") : role));
        return userRepository.save(user);
    }

    @Override
    public User saveManager(ManagerRegistrationDto registration) {
        User user = new User();
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));

        Role role = roleRepository.findByName("ROLE_MANAGER");

        user.setRoles(Collections.singletonList(role == null ? new Role("ROLE_MANAGER") : role));
        return userRepository.save(user);
    }

    @Override
    public List<User> allUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findById(Long id) {
        return userRepository.findOne(id) == null ? new User() : userRepository.findOne(id);
    }

    @Override
    public void delete(Long id){
        userRepository.findOne(id).getRoles().clear();
        userRepository.delete(id);
    }

    @Override
    public void deleteAll(){
        userRepository.deleteAll();
    }

    @Override
    public void incrementUserNumberOfTries(Long id) {

        User user = userRepository.findOne(id);

        if(user != null) {
            user.setNumberOfTries(user.getNumberOfTries() + 1);

            userRepository.save(user);
        }
    }

    @Override
    public void saveRootUser() {
        if(userRepository.findByEmail("root") != null)
            return;

        User rootUser = new User();

        rootUser.setEmail("root");
        rootUser.setPassword(passwordEncoder.encode("root"));

        Collection<Role> roles = Lists.newArrayList(
                roleRepository.findByName("ROLE_USER") == null ? new Role("ROLE_USER") : roleRepository.findByName("ROLE_USER"),
                roleRepository.findByName("ROLE_MANAGER") == null ? new Role("ROLE_MANAGER") : roleRepository.findByName("ROLE_MANAGER"),
                roleRepository.findByName("ROLE_ADMIN") == null ? new Role("ROLE_ADMIN") : roleRepository.findByName("ROLE_ADMIN"));

        rootUser.setRoles(roles);
        rootUser.setFirstName("root");
        rootUser.setLastName("root");
        userRepository.save(rootUser);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }
}
