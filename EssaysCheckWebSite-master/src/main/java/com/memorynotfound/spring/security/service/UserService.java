package com.memorynotfound.spring.security.service;

import com.memorynotfound.spring.security.model.User;
import com.memorynotfound.spring.security.web.dto.ManagerRegistrationDto;
import com.memorynotfound.spring.security.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User saveUser(UserRegistrationDto registration);

    User saveManager(ManagerRegistrationDto registration);

    List<User> allUsers();

    User findById(Long id);

    void delete(Long id);

    void deleteAll();

    void incrementUserNumberOfTries(Long id);

    void saveRootUser();
}
