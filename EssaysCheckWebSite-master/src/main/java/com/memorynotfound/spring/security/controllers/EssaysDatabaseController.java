package com.memorynotfound.spring.security.controllers;

import com.google.common.collect.Iterables;
import com.memorynotfound.spring.security.model.Essay;
import com.memorynotfound.spring.security.model.User;
import com.memorynotfound.spring.security.repository.EssayRepository;
import com.memorynotfound.spring.security.repository.UserRepository;
import com.memorynotfound.spring.security.utils.FileUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Controller
@RequestMapping("/essaysDatabase")
public class EssaysDatabaseController {

    private final EssayRepository essayRepository;

    private final UserRepository userRepository;

    private final String PATH = "/Users/mukhin/Desktop/files/";

    public EssaysDatabaseController(EssayRepository essayRepository, UserRepository userRepository) {
        this.essayRepository = essayRepository;
        this.userRepository = userRepository;
    }

    @GetMapping
    @PreAuthorize("hasRole('MANAGER')")
    public String getEssaysDatabase(Model model) {
        List<User> users = userRepository.findAll().stream().filter(it -> it.getNumberOfTries() > 0).collect(Collectors.toList());
        List<String> essays = users.stream().map(it -> createCheckQuery(Iterables.getLast(essayRepository.findByUserId(it.getId())))).collect(Collectors.toList());

        model.addAttribute("users", users);
        model.addAttribute("essays", essays);

        return "essaysDatabase";
    }

    @GetMapping(value = "/upload/{user_id}", produces = "application/zip")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity<StreamingResponseBody> downloadUserEssay(@PathVariable(name = "user_id") Long userId){

        List<Essay> essays = essayRepository.findByUserId(userId).stream().sorted(Comparator.comparingInt(Essay::getTryNumber)).collect(Collectors.toList());
        List<File> files = essays.stream().map(it -> new File(PATH + it.getFilename())).collect(Collectors.toList());

        User user = userRepository.findOne(userId);

        return ResponseEntity
                .ok()
                .header("Content-Disposition", "attachment; filename=" +
                        String.format("%s_%s_essays", user.getFirstName(), user.getLastName()))
                .contentType(MediaType.parseMediaType("application/zip"))
                .body(out -> {
                    ZipOutputStream zipOutputStream = new ZipOutputStream(out);

                    for(int i = 0; i < essays.size(); i++){
                        zipOutputStream.putNextEntry(new ZipEntry(String.format("%s_%s_essay_%d",
                                user.getFirstName(), user.getLastName(), essays.get(i).getTryNumber())
                                + FileUtils.getFileExtension(files.get(i).getName())));
                        FileInputStream fileInputStream = new FileInputStream(files.get(i));

                        IOUtils.copy(fileInputStream, zipOutputStream);

                        fileInputStream.close();
                        zipOutputStream.closeEntry();
                    }

                    zipOutputStream.close();
                });
    }

    private String createCheckQuery(Essay essay){
        StringBuilder sb = new StringBuilder();

        sb.append("1) ");
        sb.append(essay.isFirstStep() ? "Yes" : "No");
        sb.append("; ");

        sb.append("2) ");
        sb.append(essay.isSecondStep() ? "Yes" : "No");
        sb.append("; ");

        sb.append("3) ");
        sb.append(essay.isThirdStep() ? "Yes" : "No");
        sb.append("; ");

        sb.append("4) ");
        sb.append(essay.isFourthStep() ? "Yes" : "No");
        sb.append("; ");

        sb.append("5) ");
        sb.append(essay.isFifthStep() ? "Yes" : "No");
        sb.append("; ");

        return sb.toString();
    }

}
