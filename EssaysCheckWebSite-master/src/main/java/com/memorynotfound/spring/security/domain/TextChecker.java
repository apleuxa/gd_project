package com.memorynotfound.spring.security.domain;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.memorynotfound.spring.security.model.EssayWord;
import com.memorynotfound.spring.security.repository.EssayWordRepository;
import com.memorynotfound.spring.security.utils.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TextChecker {

    private static final String PARAGRAPH_SPLIT_REGEX = "\n"; //(^\s{4})

    private static final int MIN_NUMBER_OF_PARAGRAPHS = 4;

    private static final int MAX_NUMBER_OF_PARAGRAPHS = 5;

    private static final int MIN_NUMBER_OF_WORDS = 140;

    private static final int MAX_NUMBER_OF_WORDS = 150;

    private static final List<String> requiredWords = Lists.newArrayList(
            "therefore", "secondly", "thirdly", "finally", "though", "although", "besides",
            "moreover", "nevertheless", "furthermore", "however", "perhaps", "thus", "therefore",
            "in conclusion", "to sum up", "to begin with", "on the one hand", "on the other hand"
    );

    private static final int MIN_NUMBER_OF_REQUIRED_WORDS = 4;

    private static final int MAX_NUMBER_OF_MISTAKES = 8;

    private String text;

    private EssayWordRepository essayWordRepository;

    public TextChecker(String text) {
        this.text = text;
        essayWordRepository = SpringUtils.getBean(EssayWordRepository.class);
        essayWordRepository.save(requiredWords.stream().map(EssayWord::new).collect(Collectors.toList()));
    }

    private boolean isSymbol(char symbol) {
        return symbol != '\n' && symbol != '\t' && symbol != '\r';
    }

    private boolean isEndOfSentence(char symbol){
        return symbol == '.' || symbol == '!' || symbol == '?';
    }

    public int getNumberOfWords() {
        int wordCounter = 0;
        String[] words = text.split(" ");
        for (String word : words) {
            if (!word.isEmpty() && isSymbol(word.charAt(0))) {
                wordCounter++;
            }
        }

        return wordCounter;
    }

    public int getNumberOfParagraphs() {
        return text.split(PARAGRAPH_SPLIT_REGEX).length;
    }

    public boolean isWordsInText(String words) {
        String lowText = text.toLowerCase();

        return lowText.lastIndexOf(words.toLowerCase()) != -1;
    }

    public int getNumberOfElementaryMistakes(){
        int numberOfMistakes = 0;

        //Every sentence begins with large letter

        boolean endOfSentence = false;

        for(int i = 0; i < text.length(); i++){

            char symbol = text.charAt(i);

            if(!isSymbol(symbol))
                continue;

            if(isEndOfSentence(symbol)){
                endOfSentence = true;
            }else if(endOfSentence && symbol != ' '){
                if(Character.isLowerCase(symbol))
                    numberOfMistakes++;
                endOfSentence = false;
            }
        }

        return numberOfMistakes;
    }

    public boolean checkFirstStep(){
        int number = getNumberOfWords();
        return number >= MIN_NUMBER_OF_WORDS && number <= MAX_NUMBER_OF_WORDS;
    }

    public boolean checkSecondStep(){
        int number = getNumberOfParagraphs();
        return number >= MIN_NUMBER_OF_PARAGRAPHS && number <= MAX_NUMBER_OF_PARAGRAPHS;
    }

    public boolean checkThirdStep(){
        //anti-plagiarism
        return true;
    }

    public boolean checkFourthStep(){
        List<String> words = essayWordRepository.findAll().stream().map(EssayWord::getWord).collect(Collectors.toList());

        return words.stream().filter(this::isWordsInText).count() >= MIN_NUMBER_OF_REQUIRED_WORDS;
    }

    public boolean checkFifthStep(){
        return getNumberOfElementaryMistakes() <= MAX_NUMBER_OF_MISTAKES;
    }
}
