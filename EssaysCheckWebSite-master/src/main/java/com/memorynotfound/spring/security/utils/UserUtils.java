package com.memorynotfound.spring.security.utils;

import com.memorynotfound.spring.security.service.UserService;
import com.memorynotfound.spring.security.model.User;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public class UserUtils {

    public static Optional<User> getCurrentAuthenticatedUser(UserService userRepository){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if ((!(auth instanceof AnonymousAuthenticationToken)) && auth != null) {
            UserDetails userDetails = (UserDetails) auth.getPrincipal();

            if(userDetails != null){
                return Optional.of(userRepository.findByEmail(userDetails.getUsername()));
            }

        }

        return Optional.empty();
    }

    public static Optional<UserDetails> getCurrentAuthenticatedUserDetails(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if ((!(auth instanceof AnonymousAuthenticationToken)) && auth != null) {
            UserDetails userDetails = (UserDetails) auth.getPrincipal();

            if(userDetails != null){
                return Optional.of(userDetails);
            }

        }

        return Optional.empty();
    }
}
