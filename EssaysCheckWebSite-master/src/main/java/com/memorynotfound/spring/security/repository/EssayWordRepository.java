package com.memorynotfound.spring.security.repository;

import com.memorynotfound.spring.security.model.EssayWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EssayWordRepository extends JpaRepository<EssayWord, Long> {

}
