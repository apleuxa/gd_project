package com.memorynotfound.spring.security.controllers;

import com.memorynotfound.spring.security.model.User;
import com.memorynotfound.spring.security.service.UserServiceImpl;
import com.memorynotfound.spring.security.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@Controller
public class MainController {

    private final UserServiceImpl userService;

    public MainController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String root() {
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login() {
        if(UserUtils.getCurrentAuthenticatedUserDetails().isPresent())
            return "redirect:/direct";

        userService.saveRootUser();
        return "login";
    }

    @GetMapping("/direct")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_MANAGER', 'ROLE_ADMIN')")
    public String direct(Model model){

        Optional<UserDetails> userDetailsHolder = UserUtils.getCurrentAuthenticatedUserDetails();

        if(userDetailsHolder.isPresent()) {

            UserDetails userDetails = userDetailsHolder.get();

            if (userDetails.getAuthorities().stream().anyMatch(it -> it.getAuthority().equals("ROLE_ADMIN"))) {
                return "redirect:/admin";
            } else if (userDetails.getAuthorities().stream().anyMatch(it -> it.getAuthority().equals("ROLE_MANAGER"))) {
                model.addAttribute("email", userDetails.getUsername());
                return "redirect:/essaysDatabase";
            } else if (userDetails.getAuthorities().stream().anyMatch(it -> it.getAuthority().equals("ROLE_USER"))) {
                return "redirect:/userUploadEssay";
            }
        }

        //never happens
        return "redirect:/index";
    }

    @GetMapping("/user")
    public String userIndex() {
        return "user/index";
    }
}
