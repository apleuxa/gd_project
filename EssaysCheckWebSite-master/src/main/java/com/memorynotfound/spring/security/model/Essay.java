package com.memorynotfound.spring.security.model;

import javax.persistence.*;

@Entity
public class Essay {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String filename;

    private Long userId;

    private int tryNumber;

    private boolean firstStep;

    private boolean secondStep;

    private boolean thirdStep;

    private boolean fourthStep;

    private boolean fifthStep;

    public Essay(){

    }

    public Essay(String filename, Long userId, int tryNumber, boolean firstStep, boolean secondStep,
                 boolean thirdStep, boolean fourthStep, boolean fifthStep){
        this.filename = filename;
        this.userId = userId;
        this.tryNumber = tryNumber;
        this.firstStep = firstStep;
        this.secondStep = secondStep;
        this.thirdStep = thirdStep;
        this.fourthStep = fourthStep;
        this.fifthStep = fifthStep;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getTryNumber() {
        return tryNumber;
    }

    public void setTryNumber(int tryNumber) {
        this.tryNumber = tryNumber;
    }

    public boolean isFirstStep() {
        return firstStep;
    }

    public void setFirstStep(boolean firstStep) {
        this.firstStep = firstStep;
    }

    public boolean isSecondStep() {
        return secondStep;
    }

    public void setSecondStep(boolean secondStep) {
        this.secondStep = secondStep;
    }

    public boolean isThirdStep() {
        return thirdStep;
    }

    public void setThirdStep(boolean thirdStep) {
        this.thirdStep = thirdStep;
    }

    public boolean isFourthStep() {
        return fourthStep;
    }

    public void setFourthStep(boolean fourthStep) {
        this.fourthStep = fourthStep;
    }

    public boolean isFifthStep() {
        return fifthStep;
    }

    public void setFifthStep(boolean fifthStep) {
        this.fifthStep = fifthStep;
    }
}
