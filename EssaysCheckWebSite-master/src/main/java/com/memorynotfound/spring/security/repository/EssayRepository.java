package com.memorynotfound.spring.security.repository;

import com.memorynotfound.spring.security.model.Essay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EssayRepository extends JpaRepository<Essay, Long> {

    @Query("select e from Essay e where user_id = :userId")
    List<Essay> findByUserId(@Param("userId") Long userId);

}
