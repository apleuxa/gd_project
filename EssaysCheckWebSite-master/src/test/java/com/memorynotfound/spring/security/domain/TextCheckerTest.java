package com.memorynotfound.spring.security.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TextCheckerTest {

    @Test
    void getNumberOfWordsTwo() {
        assertEquals(new TextChecker("Hello world!").getNumberOfWords(), 2);
    }

    @Test
    void getNumberOfParagraphs() {
        assertEquals(new TextChecker("Hello\nworld!").getNumberOfParagraphs(), 2);
    }

    @Test
    void isWordsInText() {
        assertTrue(new TextChecker("Therefore, hello world!").isWordsInText("therefore"));
    }

    @Test
    void mistakesCheck(){
        assertEquals(new TextChecker("Hello world! hello world. hi people").getNumberOfElementaryMistakes(), 2);
        assertEquals(new TextChecker("Hello world!! hello world.. hi people").getNumberOfElementaryMistakes(), 2);
        assertEquals(new TextChecker("Hello world!  hello world!? hi people").getNumberOfElementaryMistakes(), 2);
    }

    @Test
    void noMistakesCheck(){
        assertEquals(new TextChecker("Hello world! Hello world. Hi people").getNumberOfElementaryMistakes(), 0);
    }

}